<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="../../assets/jquery.min.js"></script>
<script type="text/javascript">
	$(function() {
		$.get("list", function(data) {
			$.each(data, function(index, obj) {
				$("#operators").append("<li>" + obj.usercode + "</li>");
			});
		});
	});
</script>
</head>
<body>
	<h1>this is ${page} page.</h1>
	<ul id="operators"></ul>
</body>
</html>