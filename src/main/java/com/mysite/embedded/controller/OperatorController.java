package com.mysite.embedded.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysite.embedded.po.AcOperator;
import com.mysite.embedded.service.OperatorService;

@Controller
@RequestMapping("/operator")
public class OperatorController {

	@Autowired
	private OperatorService operatorService;

	@RequestMapping(value = "/index")
	public String operatorsIndex(ModelMap modelMap) {
		modelMap.put("page", "operator");
		return "operators";
	}
	
	@RequestMapping(value = "/list")
	@ResponseBody
	public List<AcOperator> getOperators() {
		return operatorService.getOperators();
	}
}
