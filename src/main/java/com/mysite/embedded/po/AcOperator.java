package com.mysite.embedded.po;

import java.util.Date;

/**
 * 操作员表
 * 
 * @author congge
 * @created 2017-11-17 13:26:58
 * @version v1.0.0
 */
public class AcOperator {
	/**
	 * 字段：操作员id
	 */
	private Integer id;

	/**
	 * 字段：登录帐号
	 */
	private String usercode;

	/**
	 * 字段：操作员姓名
	 */
	private String username;

	/**
	 * 字段：加密后的密码
	 */
	private String password;

	/**
	 * 字段：操作员状态: 0 正常; 1 冻结
	 */
	private Integer status;

	/**
	 * 字段：联系电话
	 */
	private String telephone;

	/**
	 * 字段：电子邮件
	 */
	private String email;

	/**
	 * 字段：所属机构id
	 */
	private Integer orgId;

	/**
	 * 字段：创建者id
	 */
	private Integer createOperId;

	/**
	 * 字段：创建时间
	 */
	private Date createTime;

	/**
	 * 读取：操作员id
	 *
	 * @return ac_operator.id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置：操作员id
	 *
	 * @param id
	 *            ac_operator.id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 读取：登录帐号
	 *
	 * @return ac_operator.usercode
	 */
	public String getUsercode() {
		return usercode;
	}

	/**
	 * 设置：登录帐号
	 *
	 * @param usercode
	 *            ac_operator.usercode
	 */
	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}

	/**
	 * 读取：操作员姓名
	 *
	 * @return ac_operator.username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * 设置：操作员姓名
	 *
	 * @param username
	 *            ac_operator.username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * 读取：加密后的密码
	 *
	 * @return ac_operator.password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 设置：加密后的密码
	 *
	 * @param password
	 *            ac_operator.password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 读取：操作员状态: 0 正常; 1 冻结
	 *
	 * @return ac_operator.status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * 设置：操作员状态: 0 正常; 1 冻结
	 *
	 * @param status
	 *            ac_operator.status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 读取：联系电话
	 *
	 * @return ac_operator.telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * 设置：联系电话
	 *
	 * @param telephone
	 *            ac_operator.telephone
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * 读取：电子邮件
	 *
	 * @return ac_operator.email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 设置：电子邮件
	 *
	 * @param email
	 *            ac_operator.email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 读取：所属机构id
	 *
	 * @return ac_operator.org_id
	 */
	public Integer getOrgId() {
		return orgId;
	}

	/**
	 * 设置：所属机构id
	 *
	 * @param orgId
	 *            ac_operator.org_id
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	/**
	 * 读取：创建者id
	 *
	 * @return ac_operator.create_oper_id
	 */
	public Integer getCreateOperId() {
		return createOperId;
	}

	/**
	 * 设置：创建者id
	 *
	 * @param createOperId
	 *            ac_operator.create_oper_id
	 */
	public void setCreateOperId(Integer createOperId) {
		this.createOperId = createOperId;
	}

	/**
	 * 读取：创建时间
	 *
	 * @return ac_operator.create_time
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * 设置：创建时间
	 *
	 * @param createTime
	 *            ac_operator.create_time
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}