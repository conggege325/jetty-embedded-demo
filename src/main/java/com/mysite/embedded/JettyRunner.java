package com.mysite.embedded;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.webapp.WebAppContext;

public class JettyRunner {
	public static void main(String[] args) throws Exception {

		int port = 8080;
		if(args.length > 0) {
			port = Integer.parseInt(args[0]);
		}
		
		WebAppContext webapp = new WebAppContext();
		webapp.setContextPath("/");
		webapp.setBaseResource(Resource.newClassPathResource("/META-INF/web"));

		Server server = new Server(port);
		server.setHandler(webapp);
		server.start();
		server.dumpStdErr();
		server.join();
	}
}
