package com.mysite.embedded.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.mysite.embedded.po.AcOperator;

@Service
public class OperatorService {

	public List<AcOperator> getOperators() {
		ArrayList<AcOperator> operators = new ArrayList<AcOperator>();
		int length = (int) (Math.random() * 10);
		for (int i = 1; i < length; i++) {
			AcOperator operator = new AcOperator();
			operator.setId(i);
			operator.setUsercode("user_" + i);
			operators.add(operator);
		}
		return operators;
	}
}
